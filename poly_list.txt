<div id="triangle" class="poly_choi" onclick="poly_choi_click(id)"><i class="fas fa-caret-up fa-lg"></i>三角形</div>
<div id="rectangle" class="poly_choi" onclick="poly_choi_click(id)"><i class="fas fa-square"></i>矩形</div>
<div id="circle" class="poly_choi" onclick="poly_choi_click(id)"><i class="fas fa-circle"></i>圓形</div>
<br>
<p id="poly_color" onclick="p_c_click()"><i class="fas fa-palette"></i>color</p>
<p id="poly_fill" onclick="fill_check()"><i class="fas fa-fill"></i>fill</p>
<p id="poly_size" onclick="p_s_click()"><i class="fas fa-expand-arrows-alt"></i>size</p>
<!-- <label class="switch">
    <input type="checkbox" onclick="fill_check()">
    <span class="slider round"></span>
</label> -->