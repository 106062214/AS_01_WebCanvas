var canvas = document.getElementById("can");
var ctx = canvas.getContext("2d");
var cPushArray = new Array();
var cStep = -1;
var bru_col_app;
var bru_size_app;
var bru_app;
var bru_size = 1;
var bru_btn_col = "black";
var bru_col = "black";
var era_size_app;
var era_app;
var era_size = 1;
var era_pre_draw = false;
var not_erasing = true;
var text_app;
var text = "";
var text_col_app;
var text_size_app;
var text_style_app;
var text_size = "20px";
var text_style = "Arial";
var text_col = "black";
var pre_draw = false;
var canvas_using = false;
var poly_app;
var poly_col_app;
var poly_size_app;
var poly_size = 5;
var poly_col = "black";
var poly_fill = false;
var using_tri;
var using_rec;
var using_cir;
var start_point;
var draw_poly_tmp;
var upload_app;
var tmp_prev = new Image();
var left_div_show = false;

class ver {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
//canvas start
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

function mouseMove(evt) {
    var mousePos = getMousePos(canvas, evt);
    if (bru_app || era_app) {
        if (era_app&&not_erasing) {
            if (!era_pre_draw) {
                tmp_prev.src = canvas.toDataURL();
            } else {
                c_clear();
                ctx.drawImage(tmp_prev, 0, 0);
            }
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            ctx.arc(mousePos.x, mousePos.y, era_size / 2, 0, 2 * Math.PI, 0);
            ctx.fill();
            ctx.strokeStyle = "balck";
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            // ctx.arc(mousePos.x, mousePos.y, era_size / 2, 0, 2 * Math.PI, 0);
            // ctx.stroke();
            ctx.strokeStyle = "white";
            era_pre_draw = true;

        }
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
    }
    if (poly_app) {
        if (draw_poly_tmp == false) {
            tmp_prev.src = canvas.toDataURL();
        }
        if (draw_poly_tmp) {
            c_clear();
            ctx.drawImage(tmp_prev, 0, 0);
        }
        draw_poly(mousePos);
        draw_poly_tmp = true;
    }
    if (text_app) {
        if (!pre_draw) {
            tmp_prev.src = canvas.toDataURL();
        } else {
            c_clear();
            ctx.drawImage(tmp_prev, 0, 0);
        }
        ctx.fillText(text, mousePos.x, mousePos.y);
        pre_draw = true;
    }
}

canvas.addEventListener("mousedown", function (evt) {
    var mousePos = getMousePos(canvas, evt);
    canvas_using = false;
    if (bru_app || era_app) {
        canvas_using = true;
        ctx.beginPath();
        if (bru_app) {
            ctx.arc(mousePos.x, mousePos.y, bru_size / 2, 0, 2 * Math.PI, 0);
        }
        if (era_app) {
            c_clear();
            ctx.drawImage(tmp_prev, 0, 0);
            not_erasing=false;
            ctx.arc(mousePos.x, mousePos.y, era_size / 2, 0, 2 * Math.PI, 0);
        }
        ctx.fill();
        ctx.beginPath();
        ctx.moveTo(mousePos.x, mousePos.y);
        evt.preventDefault();
        canvas.addEventListener("mousemove", mouseMove, false);
    }
    if (text_app) {
        canvas_using = true;
        // c_clear();
        // ctx.drawImage(tmp_prev, 0, 0);
        draw_poly_tmp = false;
        // ctx.font = "black 48px serif";
        ctx.fillText(text, mousePos.x, mousePos.y);
        tmp_prev.src = canvas.toDataURL();
    }
    if (poly_app) {
        canvas_using = true;
        start_point = mousePos;
        canvas.addEventListener("mousemove", mouseMove, false);
    }
});

canvas.addEventListener(
    "mouseup",
    function (evt) {
        var mousePos = getMousePos(canvas, evt);
        if (canvas_using) {
            if (poly_app) {
                canvas.removeEventListener("mousemove", mouseMove, false);
                if (draw_poly_tmp) {
                    c_clear();
                    ctx.drawImage(tmp_prev, 0, 0);
                    draw_poly_tmp = false;
                }
                draw_poly(mousePos);
            }
            if (bru_app) {
                canvas.removeEventListener("mousemove", mouseMove, false);
                canvas_using = true;
                ctx.beginPath();
                ctx.arc(mousePos.x, mousePos.y, bru_size / 2, 0, 2 * Math.PI, 0);
                ctx.fill();
            }
            if (era_app) {
                // canvas.removeEventListener("mousemove", mouseMove, false);
                canvas_using = true;
                ctx.beginPath();
                ctx.arc(mousePos.x, mousePos.y, era_size / 2, 0, 2 * Math.PI, 0);
                ctx.fill();
                not_erasing=true;
            }
            canvas_using = false;
        }
        cPush();
    },
    false
);
//canvas end

//reset start
function c_clear() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function r_c_clear() {
    c_clear();
    tmp_prev.src = canvas.toDataURL();
    cPush();
}
document.getElementById("res").addEventListener("click", r_c_clear, false);
//reset end

//mutual start
$(document).ready(function () {
    //initialize
    $(".buttons").hide();
    ctx.strokeStyle = "black";
    ctx.lineWidth = "1";
    $("#r").hide();
    bru_col_app = false;
    era_app = false;
    bru_app = true;
    bru_size_app = false;
    era_size_app = false;
    text_app = false;
    text_col_app = false;
    text_size_app = false;
    text_style_app = false;
    poly_app = false;
    poly_col_app = false;
    poly_size_app = false;
    using_tri = true;
    using_rec = false;
    using_cir = false;
    draw_poly_tmp = false;
    upload_app = false;
    $("#leftdiv").hide();
});

function ini_nav_color() {
    console.log("hihi");
    $("#nav_1").css({
        color: "black"
    });
    $("#nav_2").css({
        color: "black"
    });
    $("#nav_3").css({
        color: "black"
    });
    $("#nav_4").css({
        color: "black"
    });
    $("#nav_5").css({
        color: "black"
    });
    $("#nav_6").css({
        color: "black"
    });
    $("#nav_7").css({
        color: "black"
    });
    $("#nav_8").css({
        color: "black"
    });
    $("#nav_9").css({
        color: "black"
    });
}

function ini_l_color() {
    $("#l").css({
        color: 'black'
    })
}

// var change_bru_gra_col;

function right_toggle() {
    ini_l_color();
    if (bru_col_app) {
        $("#r").load("color_button.txt");
        ini_l_color();
        console.log("1");
        $("#bru_size").css({
            color: 'black'
        })
        $("#bru_color").css({
            color: 'white'
        })
        var ctx_col
        // setTimeout(function () {
        //     var canvas_col = document.getElementById("color_bar");
        //     ctx_col = canvas_col.getContext("2d");
        //     var gradient = ctx_col.createLinearGradient(0, 0, 0, 50);

        //     // Add three color stops
        //     change_bru_gra_col=function () {
        //         gradient.addColorStop(0, bru_btn_col);
        //         gradient.addColorStop(.99, 'black');
        //         ctx_col.fillStyle = gradient;
        //         ctx_col.fillRect(0, 0, 200, 50);
        //     }
        //     change_bru_gra_col();
        //     function pick(event) {
        //         var x = event.layerX;
        //         var y = event.layerY;
        //         var pixel = ctx_col.getImageData(x, y, 1, 1);
        //         var data = pixel.data;
        //         var rgba = 'rgba(' + data[0] + ', ' + data[1] + ', ' + data[2] + ', ' + (data[3] / 255) + ')';
        //         bru_col = rgba;
        //         ctx.strokeStyle = bru_col;
        //         ctx.fillStyle = bru_col;
        //         bru_col = bru_col;
        //         console.log(x,y);
        //     }
        //     canvas_col.addEventListener('click', pick);
        // }, 30)
    } else if (bru_size_app) {
        $("#r").load("size_button.txt");
        setTimeout(function () {
            $("#siz_ran").attr({
                    value: bru_size
                }),
                $("#siz_ran").css({
                    height: bru_size
                });
        }, 10);
        ini_l_color();
        console.log("2");
        $("#bru_color").css({
            color: 'black'
        })
        $("#bru_size").css({
            color: 'white'
        })
    } else if (era_size_app) {
        $("#r").load("era_size_button.txt");
        setTimeout(function () {
            $("#era_siz_ran").attr({
                    value: era_size
                }),
                $("#era_siz_ran").css({
                    height: era_size
                });
        }, 10);
        ini_l_color();
        $("#era_size").css({
            color: 'white'
        })
    } else if (text_col_app) {
        $("#r").load("text_color_button.txt");
        $("#text_style").css({
            color: 'black'
        })
        $("#text_size").css({
            color: 'black'
        })
        $("#text_color").css({
            color: 'white'
        })
    } else if (text_size_app) {
        $("#r").load("text_size_button.txt");
        setTimeout(function () {
            $("#text_siz_ran").attr({
                value: 20
            });
            var ta = document.getElementById("text_A");
            ta.style.fontSize = text_size;
        }, 10);
        $("#text_style").css({
            color: 'black'
        })
        $("#text_color").css({
            color: 'black'
        })
        $("#text_size").css({
            color: 'white'
        })
    } else if (text_style_app) {
        $("#r").load("text_style_button.txt");
        $("#text_color").css({
            color: 'black'
        })
        $("#text_size").css({
            color: 'black'
        })
        $("#text_style").css({
            color: 'white'
        })
    } else if (poly_col_app) {
        $("#r").load("poly_color_button.txt");
        $("#poly_size").css({
            color: 'black'
        })
        $("#poly_color").css({
            color: 'white'
        })
    } else if (poly_size_app) {
        $("#r").load("poly_size_button.txt");
        setTimeout(function () {
            $("#poly_siz_ran").attr({
                    value: poly_size
                }),
                $("#poly_siz_ran").css({
                    height: poly_size
                });
        }, 10);
        $("#poly_color").css({
            color: 'black'
        })
        $("#poly_size").css({
            color: 'white'
        })
    } else {
        console.log("!!!!")
        $("#bru_size").css({
            color: 'black'
        })
        $("#bru_color").css({
            color: 'black'
        })
        $("#era_size").css({
            color: 'black'
        })
        $("#text_style").css({
            color: 'black'
        })
        $("#text_color").css({
            color: 'black'
        })
        $("#text_size").css({
            color: 'black'
        })
        $("#poly_size").css({
            color: 'black'
        })
        $("#poly_color").css({
            color: 'black'
        })
    }
}

function right_panel_handler() {
    ini_l_color();
    if (
        bru_col_app ||
        bru_size_app ||
        era_size_app ||
        text_col_app ||
        text_style_app ||
        text_size_app ||
        poly_col_app ||
        poly_size_app
    ) {
        $("#r").hide();
        $("#r").empty();
        $("#r").show(250);
    }
    setTimeout(function () {
        right_toggle();
    }, 70);
}

function control_left_div() {
    // left_div_show = !left_div_show;
    // console.log(left_div_show);
    $("#leftdiv").hide()
    $("#leftdiv").show(350)
}
//mutual end

//brush start
function col_pick(col) {
    bru_btn_col = col;
    ctx.strokeStyle = col;
    ctx.fillStyle = col;
    bru_col = col;
    // change_bru_gra_col();
}

function b_c_click() {
    bru_col_app = !bru_col_app;
    bru_size_app = false;
    if (bru_col_app == false) {
        $("#r").hide(250);
        setTimeout(function () {
            $("#r").empty();
        }, 200);
    }
    right_panel_handler();


}


function b_s_click() {
    bru_size_app = !bru_size_app;
    bru_col_app = false;
    if (bru_size_app == false) {
        $("#r").hide(250);
        setTimeout(function () {
            $("#r").empty();
        }, 200);
    }
    right_panel_handler();
}

function size_range_cli(value) {
    ctx.lineWidth = value;
    $("#siz_ran").css({
        height: value
    });
    bru_size = value;
}

$("#brush").click(function () {
    era_size_app = false;
    era_app = false;
    bru_col_app = false;
    bru_size_app = false;
    text_app = false;
    text_col_app = false;
    text_size_app = false;
    text_style_app = false;
    poly_col_app = false;
    poly_size_app = false;
    poly_app = false;
    upload_app = false;
    bru_app = true;
    control_left_div();
    if (bru_app) {
        ini_nav_color();
        $("#nav_1").css({
            color: "white"
        });
    }
    $("#l").load("brush_list.txt");
    $("#l").css({
        width: "50%"
    });
    $("#r").empty();
    $("#r").hide(150);
    $("#can").css({
        cursor: "url(./pen_re.png), default"
    });
    ctx.fillStyle = bru_col;
    ctx.strokeStyle = bru_col;
    ctx.lineWidth = bru_size;
});
//brush end

//eraser start
function e_s_click() {
    era_size_app = !era_size_app;
    if (era_size_app == false) {
        $("#r").hide(250);
        setTimeout(function () {
            $("#r").empty();
        }, 200);
    }
    right_panel_handler();
}

function era_size_range_cli(value) {
    ctx.lineWidth = value;
    $("#era_siz_ran").css({
        height: value
    });
    era_size = value;
}

$("#eraser").click(function () {
    bru_col_app = false;
    bru_app = false;
    bru_size_app = false;
    era_size_app = false;
    text_app = false;
    text_col_app = false;
    text_style_app = false;
    text_size_app = false;
    poly_col_app = false;
    poly_size_app = false;
    poly_app = false;
    upload_app = false;
    era_app = true;
    control_left_div()
    if (era_app) {
        ini_nav_color();
        $("#nav_2").css({
            color: "white"
        });
    }
    $("#l").load("eraser_list.txt");
    $("#l").css({
        width: "50%"
    });
    $("#r").empty();
    $("#r").hide(150);
    $("#can").css({
        cursor: "url(./reaser_re.png), default"
    });
    ctx.fillStyle = "white";
    ctx.strokeStyle = "white";
    ctx.lineWidth = era_size;
    canvas.addEventListener("mousemove", mouseMove, false);
});
//eraser end

//typing start
function text_col_pick(col) {
    ctx.fillStyle = col;
    text_col = col;
}

function text_size_range_cli(value) {
    text_size = value + "px";
    ctx.font = text_size + " " + text_style;
    // console.log(text_size+" "+text_style);
    var ta = document.getElementById("text_A");
    ta.style.fontSize = text_size;
}

function inp_tex(tmp) {
    text = tmp;
}

function t_c_click() {
    text_col_app = !text_col_app;
    text_size_app = false;
    text_style_app = false;
    if (text_col_app == false) {
        $("#r").hide(250);
        setTimeout(function () {
            $("#r").empty();
        }, 200);
    }
    right_panel_handler();
}

function t_s_click() {
    text_size_app = !text_size_app;
    text_style_app = false;
    text_col_app = false;
    if (text_size_app == false) {
        $("#r").hide(250);
        setTimeout(function () {
            $("#r").empty();
        }, 200);
    }
    right_panel_handler();
}

function t_style_click() {
    text_style_app = !text_style_app;
    text_size_app = false;
    text_col_app = false;
    if (text_style_app == false) {
        $("#r").hide(250);
        setTimeout(function () {
            $("#r").empty();
        }, 200);
    }
    right_panel_handler();
}

function change_font_style(sty) {
    if (sty == "Arial") {
        text_style = "Arial";
    }
    if (sty == "Germania_One") {
        text_style = "Germania One";
    }
    if (sty == "Lato") {
        text_style = "Lato";
    }
    if (sty == "Raleway") {
        text_style = "Raleway";
    }
    if (sty == "Merriweather") {
        text_style = "Merriweather";
    }
    if (sty == "Roboto_Slab") {
        text_style = "Roboto Slab";
    }
    if (sty == "Open_Sans_Condensed") {
        text_style = "Open Sans Condensed";
    }
    if (sty == "Playfair_Display") {
        text_style = "Playfair Display";
    }
    console.log(text_style);
    ctx.font = text_size + " " + text_style;
}

$("#typing").click(function () {
    bru_col_app = false;
    bru_app = false;
    bru_size_app = false;
    era_size_app = false;
    era_app = false;
    text_col_app = false;
    text_size_app = false;
    text_style_app = false;
    poly_col_app = false;
    poly_size_app = false;
    poly_app = false;
    upload_app = false;
    text_app = true;
    control_left_div()
    if (text_app) {
        ini_nav_color();
        $("#nav_3").css({
            color: "white"
        });
    }
    $("#l").load("typing_list.txt");
    $("#l").css({
        width: "50%"
    });
    $("#r").empty();
    $("#r").hide(150);
    $("#can").css({
        cursor: "url(./a_re.png), default"
    });
    ctx.strokeStyle = text_col;
    ctx.fillStyle = col;
    ctx.font = text_size + " " + text_style;
    console.log(ctx.font);
    canvas.addEventListener("mousemove", mouseMove, false);
});
//typing end

//un,redo start
function cPush() {
    console.log("pu");
    console.log(cPushArray.length);
    cStep++;
    if (cStep < cPushArray.length) {
        cPushArray.length = cStep;
    }
    cPushArray.push(canvas.toDataURL());
    if (cStep > 0 && cPushArray[cStep] == cPushArray[cStep - 1]) {
        cStep--;
        cPushArray.length--;
    }
    tmp_prev.src = canvas.toDataURL();
}

function cUndo() {
    if (cStep == 0) {
        c_clear();
        cStep--;
    }
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () {
            c_clear();
            ctx.drawImage(canvasPic, 0, 0);
        };
        console.log("un");
    } else {
        cStep = -1;
    }
    setTimeout(function () {
        tmp_prev.src = canvas.toDataURL();
    }, 50);
    // c_clear();
}

function cRedo() {
    console.log("re");
    if (cStep < cPushArray.length - 1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () {
            c_clear();
            ctx.drawImage(canvasPic, 0, 0);
        };
    }
    setTimeout(function () {
        tmp_prev.src = canvas.toDataURL();
    }, 50);

}
//un,redo end

//poly start

function p_c_click() {
    poly_col_app = !poly_col_app;
    poly_size_app = false;
    if (poly_col_app == false) {
        $("#r").hide(250);
        setTimeout(function () {
            $("#r").empty();
        }, 200);
    }
    right_panel_handler();
}

function p_s_click() {
    poly_size_app = !poly_size_app;
    poly_col_app = false;
    if (poly_size_app == false) {
        $("#r").hide(250);
        setTimeout(function () {
            $("#r").empty();
        }, 200);
    }
    right_panel_handler();
}

function poly_col_pick(col) {
    ctx.strokeStyle = col;
    ctx.fillStyle = col;
    poly_col = col;
}

function poly_size_range_cli(value) {
    ctx.lineWidth = value;
    $("#poly_siz_ran").css({
        height: value
    });
    poly_size = value;
}

function poly_choi_click(id) {
    if (id == "triangle") {
        using_tri = true;
        using_rec = false;
        using_cir = false;
        $("#rectangle").css({
            color: 'black'
        })
        $("#circle").css({
            color: 'black'
        })
        $("#triangle").css({
            color: 'white'
        })
    } else if (id == "rectangle") {
        using_tri = false;
        using_rec = true;
        using_cir = false;
        $("#triangle").css({
            color: 'black'
        })
        $("#circle").css({
            color: 'black'
        })
        $("#rectangle").css({
            color: 'white'
        })
    } else if (id == "circle") {
        using_tri = false;
        using_rec = false;
        using_cir = true;
        $("#rectangle").css({
            color: 'black'
        })
        $("#triangle").css({
            color: 'black'
        })
        $("#circle").css({
            color: 'white'
        })
    }
}

$("#poly").click(function () {
    bru_col_app = false;
    bru_app = false;
    bru_size_app = false;
    era_size_app = false;
    era_app = false;
    text_col_app = false;
    text_size_app = false;
    text_app = false;
    text_style_app = false;
    poly_col_app = false;
    poly_size_app = false;
    upload_app = false;
    poly_app = true;
    control_left_div()
    if (poly_app) {
        ini_nav_color();
        $("#nav_4").css({
            color: "white"
        });
    }
    $("#l").load("poly_list.txt");
    $("#l").css({
        width: "50%"
    });
    $("#r").empty();
    $("#r").hide(150);

    $("#can").css({
        cursor: "crosshair"
    });
    ctx.fillStyle = poly_col;
    ctx.strokeStyle = poly_col;
    ctx.lineWidth = poly_size;
});

function fill_check() {
    poly_fill = !poly_fill;
    if (poly_fill) {
        console.log("white");
        $("#poly_fill").css({
            color: 'white'
        });
    } else {
        console.log("black");
        $("#poly_fill").css({
            color: 'black'
        });
    }
}

function draw_poly(mousePos) {
    var leng;
    leng = Math.sqrt(
        Math.pow(start_point.x - mousePos.x, 2) +
        Math.pow(start_point.y - mousePos.y, 2)
    );
    if (using_tri) {
        console.log(leng);
        var a = new ver();
        a.x = start_point.x - mousePos.x;
        a.y = start_point.y - mousePos.y;
        var f1 = new ver(a.y / leng, -a.x / leng);
        var f2 = new ver(-a.y / leng, a.x / leng);
        ctx.beginPath();
        ctx.moveTo(start_point.x, start_point.y);
        ctx.lineTo(
            mousePos.x + (f1.x * leng) / Math.sqrt(3),
            mousePos.y + (f1.y * leng) / Math.sqrt(3)
        );
        ctx.lineTo(
            mousePos.x + (f2.x * leng) / Math.sqrt(3),
            mousePos.y + (f2.y * leng) / Math.sqrt(3)
        );
        ctx.closePath();
        if (!poly_fill) {
            ctx.stroke();
        } else {
            ctx.fill();
        }

    } else if (using_rec) {
        if (!poly_fill) {
            ctx.strokeRect(
                start_point.x,
                start_point.y,
                mousePos.x - start_point.x,
                mousePos.y - start_point.y
            );
        } else {
            ctx.fillRect(
                start_point.x,
                start_point.y,
                mousePos.x - start_point.x,
                mousePos.y - start_point.y
            );
        }

    } else if (using_cir) {
        console.log(leng);
        ctx.beginPath();
        ctx.arc(start_point.x, start_point.y, leng, 0, Math.PI * 2, true);
        if (!poly_fill) {
            ctx.stroke();
        } else {
            ctx.fill();
        }
    }
}
//poly end

//down,up load start

function download_image() {
    var img = canvas
        .toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    var link = document.createElement("a");
    link.download = "canvas.png";
    link.href = img;
    link.click();
}
$("#upload").click(function () {
    bru_col_app = false;
    bru_app = false;
    bru_size_app = false;
    era_size_app = false;
    era_app = false;
    text_col_app = false;
    text_size_app = false;
    text_style_app = false;
    text_app = false;
    poly_col_app = false;
    poly_size_app = false;
    poly_app = false;
    upload_app = true;
    if (upload_app) {
        ini_nav_color();
        $("#nav_9").css({
            color: "white"
        });
    }
    control_left_div()
    $("#l").load("upload_list.txt");
    $("#l").css({
        width: "200px"
    });

    $("#r").empty();
    $("#r").hide(150);
    $("#can").css({
        cursor: "default"
    });

    ctx.strokeStyle = poly_col;
    ctx.lineWidth = poly_size;
});

function upload() {
    let file = document.querySelector("input[type=file]").files[0]; // 獲取選擇的檔案，這裡是圖片型別
    let reader = new FileReader();
    reader.readAsDataURL(file); //讀取檔案並將檔案以URL的形式儲存在resulr屬性中 base64格式
    reader.onload = function (e) {
        // 檔案讀取完成時觸發
        let result = e.target.result; // base64格式圖片地址
        var image = new Image();
        image.src = result; // 設定image的地址為base64的地址

        image.onload = function () {
            var canvas = document.querySelector("#canvas");
            ctx.drawImage(image, 0, 0, image.width, image.height); // 在canvas上繪製圖片
            let dataUrl = canvas.toDataURL("image/jpeg", 0.92); // 0.92為壓縮比，可根據需要設定，設定過小會影響圖片質量
            // dataUrl 為壓縮後的圖片資源，可將其上傳到伺服器
        };
    };
}
//down,up load end