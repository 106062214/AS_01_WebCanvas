# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

1.畫面最上方的navbar選單，在畫面縮小後會變成下拉式選單，使選單內容不會擠在一起
2.上方選單最左邊的小畫家icon按下去可以重新整理。
3.每個功能的詳細選單一開始是隱藏的，在按下上方選單的功能後會從左側跳出來
4.按下上方其中一個功能後可以看到左側的選單是由上而下漸層的。
5.筆刷:按下上方筆刷後可以在左側的選單選取筆刷的顏色和大小，按下後會顯示原本隱藏的選單，接著即可選取顏色，再按下一次左邊的顏色或大小，此選單可以隱藏起來。點選左邊的size右邊會跳出拉桿，拉動啦桿，拉桿的粗細會改變，這個粗度就是會在畫布上看到的粗度，選完以後就可以在畫布上畫畫了，滑鼠移到畫布上會變成鉛筆的圖案，筆尖就是會畫到的地方。
6.橡皮擦:點選上方的橡皮擦就可以跳出橡皮擦的選單，橡皮擦只有大小可以選而以，它的用法和筆刷的一樣，將滑鼠移到畫布上後在橡皮擦圖示的前面會有一個空白的圈圈跟著滑鼠，這個可以預覽會擦到哪裡。
7.打字:按下上方的打字可以跳出打字選單，選單中可以選顏色、大小、字體。顏色的用法和筆刷的一樣，而大小的拉桿拉動後再拉桿的左下角有一個"A"，它的大小會改變，可以由此預覽打字時字會有多大。在字型選單中有8種字型可以選擇。按下即可選取字型。而在畫布上打字的方法是先在左邊選單上方打字，接著將滑鼠移到畫布上，畫布上會有現在打的字跟著滑鼠移動，點下滑鼠左鍵即可將字印在畫布上(類似印章的概念)
8.圖形:按下上方的圖形可以跳出圖形選單，有三同型可以選(三角形、矩形、圓形)，選完圖形以後可以選顏色，顏色的部分和前面的都一樣，接著可以選是否要填滿，如果要填滿就按下fill，此時fill字樣會變成白色，不果只想要線條就再按一次fill，fill字樣又會變回黑色，代表現在是線條的狀態。接著size就是決定線條狀態實現的粗細程度，用法和前面的一樣。滑鼠在畫布上點下拖意以後可以預覽現在的圖形長怎樣，'滑鼠訪開即可以放置圖形。三角形的繪製是會畫出一個正三角形，滑鼠點下去的地方是三角形其中一個頂點，拖曳以後滑鼠的位置會變成那個頂點對邊的中點，藉著拖曳就可以決定三角形大小。而繪製矩形則是滑鼠按下的那一點是長方形的左上角那點，拖曳以後滑鼠放開的點是長方形右下角的點。繪製圓形的部分，點下去的點是圓的圓心，放開的點與圓心的距離為圓的半徑。
9.清空:按下上方選單的清空即可將畫布清空。
10.undo:按下上方選單的undo即可undo。
11.redo:按下上方選單的redo即可redo。
12.下載:按下上方選單的下載即可下載畫布的內容
13.上傳:按下上方選單的上傳會跳出上傳選單，接著按下上船選單中的browse即可選取檔案，選完案開啟就可以上傳。
14.分頁icon:在網頁分頁的地方有我自己畫的icon